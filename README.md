<!--
SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

<h1 align="center">
Biography
</h1>

<p align="center"><strong>
Write the story of your life
</strong></p>

Diary application that just works.
