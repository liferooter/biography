// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::vec;

use adw::traits::AdwApplicationExt;
use gettextrs::gettext;
use log::{debug, info};

use adw::subclass::prelude::*;
use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gdk, gio, glib};

use crate::config::{APP_ID, PKGDATADIR, PROFILE, VERSION};
use crate::declare_action;
use crate::utils::get_settings;
use crate::widgets::MainWindow;

mod imp {
    use super::*;
    use glib::WeakRef;
    use once_cell::sync::OnceCell;

    #[derive(Debug, Default)]
    pub struct Application {
        pub window: OnceCell<WeakRef<MainWindow>>,
        pub accent_style: OnceCell<gtk::CssProvider>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "BiographyApplication";
        type Type = super::Application;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for Application {}

    impl ApplicationImpl for Application {
        fn activate(&self, app: &Self::Type) {
            debug!("GtkApplication<BiographyApplication>::activate");
            self.parent_activate(app);

            if let Some(window) = self.window.get() {
                let window = window.upgrade().unwrap();
                window.present();
                return;
            }

            let window = MainWindow::new(app);
            self.window
                .set(window.downgrade())
                .expect("Window already set.");

            app.main_window().present();
        }

        fn startup(&self, app: &Self::Type) {
            debug!("GtkApplication<BiographyApplication>::startup");
            self.parent_startup(app);

            // Set icons for shell
            gtk::Window::set_default_icon_name(APP_ID);

            app.setup_gactions();
            app.setup_accels();
            app.setup_styles();
        }
    }

    impl GtkApplicationImpl for Application {}
    impl AdwApplicationImpl for Application {}
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl Default for Application {
    fn default() -> Self {
        Self::new()
    }
}

impl Application {
    pub fn new() -> Self {
        glib::Object::new(&[
            ("application-id", &Some(APP_ID)),
            ("flags", &gio::ApplicationFlags::empty()),
            (
                "resource-base-path",
                &Some("/io/gnome/gitlab/liferooter/Biography/"),
            ),
        ])
        .expect("Application initialization failed...")
    }

    fn main_window(&self) -> MainWindow {
        self.imp().window.get().unwrap().upgrade().unwrap()
    }

    fn setup_gactions(&self) {
        // Quit
        declare_action!(
            "quit" in self,
            clone!(@weak self as app => move |_, _| {
                // This is needed to trigger the delete event
                // and saving the window state and current entry
                app.main_window().close();
                app.quit();
            })
        );

        // About
        declare_action!(
            "about" in self,
            clone!(@weak self as app => move |_, _| {
                app.show_about_dialog();
            })
        );
    }

    fn setup_styles(&self) {
        let settings = get_settings();
        // Bind color scheme to settings
        settings.connect_changed(
            Some("color-scheme"),
            clone!(@weak self as win => move |settings, _| win.update_color_scheme(settings)),
        );
        self.update_color_scheme(&settings);

        // Bind accent color to settings
        let css_provider = gtk::CssProvider::new();
        gtk::StyleContext::add_provider_for_display(
            &gdk::Display::default().expect("Failed to get default display"),
            &css_provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );
        self.imp()
            .accent_style
            .set(css_provider)
            .expect("Failed to save style provider");
        settings.connect_changed(
            Some("accent-color"),
            clone!(@weak self as win => move |settings, _| win.update_accent_color(settings)),
        );
        self.update_accent_color(&settings)
    }

    fn update_color_scheme(&self, settings: &gio::Settings) {
        let style_manager = self.style_manager();
        let style: String = settings.get("color-scheme");

        style_manager.set_color_scheme(match style.as_str() {
            "light" => adw::ColorScheme::ForceLight,
            "dark" => adw::ColorScheme::ForceDark,
            "system-default" => adw::ColorScheme::Default,
            _ => unreachable!(),
        });
    }

    fn update_accent_color(&self, settings: &gio::Settings) {
        let css_provider = self
            .imp()
            .accent_style
            .get()
            .expect("Failed to get accent style provider");
        let accent: String = settings.get("accent-color");
        if accent == "blue" {
            css_provider.load_from_data(&[]);
        } else {
            css_provider.load_from_data(
                format!(
                    "
                    @define-color accent_bg_color {};
                    @define-color accent_color @accent_bg_color;
                ",
                    match accent.as_str() {
                        "red" => "@red_1",
                        "orange" => "@orange_3",
                        "yellow" => "@yellow_4",
                        "green" => "@green_3",
                        "lightblue" => "@blue_2",
                        "purple" => "@purple_2",
                        _ => unreachable!(),
                    }
                )
                .as_bytes(),
            );
        }
    }

    // Sets up keyboard shortcuts
    fn setup_accels(&self) {
        let action_accels = [
            ("app.quit", vec!["<Ctrl>q"]),
            ("window.close", vec!["<Ctrl>w"]),
            ("diary.select-date", vec!["<Ctrl>l"]),
            ("diary.start-search", vec!["<Ctrl>f"]),
            ("diary.today", vec!["<Alt>Home"]),
            ("diary.preferences", vec!["<Ctrl>comma"]),
        ];

        for (action_name, accels) in action_accels {
            self.set_accels_for_action(action_name, &accels);
        }
    }

    fn show_about_dialog(&self) {
        let dialog = gtk::AboutDialog::builder()
            .logo_icon_name(APP_ID)
            .license_type(gtk::License::Gpl30)
            .website("https://gitlab.gnome.org/liferooter/biography/")
            .version(VERSION)
            .transient_for(&self.main_window())
            .translator_credits(&gettext("translator-credits"))
            .modal(true)
            .authors(vec!["Gleb Smirnov".into()])
            .artists(vec!["Gleb Smirnov".into()])
            .build();

        dialog.present();
    }

    pub fn run(&self) {
        info!("Biography ({})", APP_ID);
        info!("Version: {} ({})", VERSION, PROFILE);
        info!("Datadir: {}", PKGDATADIR);

        ApplicationExtManual::run(self);
    }
}
