// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::glib::clone;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::declare_action;

use super::MainWindow;

mod imp {
    use super::*;

    use chrono::Datelike;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/io/gnome/gitlab/liferooter/Biography/ui/calendar.ui")]
    pub struct Calendar {
        #[template_child]
        pub calendar: gtk::TemplateChild<gtk::Calendar>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Calendar {
        const NAME: &'static str = "BiographyCalendar";
        type Type = super::Calendar;
        type ParentType = adw::Window;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Calendar {
        fn constructed(&self, obj: &Self::Type) {
            // Setup actions
            obj.setup_actions();

            // Select currently selected day
            self.calendar.select_day({
                let date = self
                    .main_window()
                    .selected_date()
                    .expect("Failed to get currently selected date");

                &glib::DateTime::new(
                    &glib::TimeZone::local(),
                    date.year(),
                    date.month() as i32,
                    date.day() as i32,
                    0,
                    0,
                    0.0,
                )
                .expect("Failed to create glib::DateTime")
            });
        }
    }

    impl WidgetImpl for Calendar {}
    impl WindowImpl for Calendar {}

    impl AdwWindowImpl for Calendar {}

    #[gtk::template_callbacks]
    impl Calendar {
        pub fn main_window(&self) -> MainWindow {
            self.instance()
                .transient_for()
                .expect("Failed to get main window")
                .downcast::<MainWindow>()
                .expect("Failed to cast main window to `BiographyMainWindow`")
        }

        #[template_callback]
        fn day_selected(&self) {
            self.main_window().select_date({
                let date = self.calendar.date();
                chrono::NaiveDate::from_ymd(
                    date.year(),
                    date.month() as u32,
                    date.day_of_month() as u32,
                )
            });

            self.instance().close();
        }
    }
}

glib::wrapper! {
    pub struct Calendar(ObjectSubclass<imp::Calendar>)
        @extends gtk::Widget, gtk::Window, adw::Window;
}

impl Calendar {
    pub fn new(win: &MainWindow) -> Self {
        glib::Object::new(&[("application", &win.application()), ("transient-for", win)])
            .expect("Failed to create BiographyCalendar")
    }

    fn setup_actions(&self) {
        let group = gio::SimpleActionGroup::new();

        declare_action!(
            "today" in group,
            clone!(@weak self as win => move |_, _| {
                win
                    .imp()
                    .main_window()
                    .select_date(chrono::Local::today().naive_local());

                win.close();
            })
        );

        self.insert_action_group("calendar", Some(&group));
    }
}
