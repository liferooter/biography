// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::clone;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::declare_action;
use crate::utils::get_settings;

use super::MainWindow;

mod imp {
    use std::cell::Cell;

    use crate::{
        utils::secrets::{change_password, validate_password},
        widgets::password_page::PasswordPage,
    };

    use super::*;

    use gtk::{template_callbacks, CompositeTemplate};

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/io/gnome/gitlab/liferooter/Biography/ui/preferences.ui")]
    pub struct Preferences {
        #[template_child]
        pub password_switch: TemplateChild<gtk::Switch>,
        setup_finished: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Preferences {
        const NAME: &'static str = "BiographyPreferences";
        type Type = super::Preferences;
        type ParentType = adw::PreferencesWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template()
        }
    }

    impl ObjectImpl for Preferences {
        fn constructed(&self, obj: &Self::Type) {
            obj.setup_actions();

            // Set password switch in correct state
            self.password_switch.set_active(!validate_password(None));

            self.setup_finished.set(true);

            obj.connect_destroy(|_| log::info!("Preferences is destroyed"));
        }
    }
    impl WidgetImpl for Preferences {}
    impl WindowImpl for Preferences {}
    impl AdwWindowImpl for Preferences {}
    impl PreferencesWindowImpl for Preferences {}

    #[template_callbacks]
    impl Preferences {
        #[template_callback]
        fn toggle_password(&self, is_enable: bool) -> bool {
            if !self.setup_finished.get() {
                return false;
            }

            if is_enable {
                let win = self.instance();
                win.present_subpage(&PasswordPage::new(&self.instance()));
            } else {
                change_password(self.instance().main_window().dek(), None);
                return false;
            }

            true
        }
    }
}

glib::wrapper! {
    pub struct Preferences(ObjectSubclass<imp::Preferences>)
        @extends gtk::Widget, gtk::Window, adw::Window, adw::PreferencesWindow;
}

impl Preferences {
    pub fn new(win: &MainWindow) -> Self {
        glib::Object::new(&[("transient-for", win), ("application", &win.application())])
            .expect("Failed to create BiographyPreferences")
    }

    pub fn set_password_state(&self, is_set: bool) {
        let switch = self.imp().password_switch.get();

        switch.set_state(is_set);
        switch.set_active(is_set);
    }

    pub fn main_window(&self) -> MainWindow {
        self.transient_for()
            .expect("Failed to get main window")
            .downcast()
            .expect("Failed to downcast window to BiographyMainWindow")
    }

    fn setup_actions(&self) {
        let settings = get_settings();
        let settings_actions = ["color-scheme", "accent-color"];

        let group = gio::SimpleActionGroup::new();

        for action_name in settings_actions {
            group.add_action(&settings.create_action(action_name));
        }

        declare_action!(
            "go-back" in group,
            clone!(@weak self as win => move |_, _| {
                win.close_subpage()
            })
        );

        self.insert_action_group("prefs", Some(&group));
    }
}
