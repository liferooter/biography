// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

mod calendar;
mod password_page;
mod preferences;
mod window;

pub use window::MainWindow;
