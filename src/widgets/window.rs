// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use chrono::{Local, NaiveDate};
use glib::clone;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::application::Application;
use crate::declare_action;
use crate::utils::crypto::EncryptionKey;
use crate::utils::get_settings;
use crate::widgets::calendar::Calendar;
use crate::widgets::preferences::Preferences;

mod imp {
    use std::cell::Cell;

    use crate::{
        diary::{Entry, EntryMetadata, Storage},
        utils::{
            self,
            crypto::EncryptionKey,
            secrets::{load_key, validate_password},
        },
    };

    use super::*;

    use gtk::CompositeTemplate;
    use once_cell::unsync::OnceCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/io/gnome/gitlab/liferooter/Biography/ui/window.ui")]
    pub struct MainWindow {
        #[template_child]
        delete_diary_revealer: gtk::TemplateChild<gtk::Revealer>,
        #[template_child]
        password_listbox: gtk::TemplateChild<gtk::ListBox>,
        #[template_child]
        password_row: gtk::TemplateChild<gtk::Editable>,
        #[template_child]
        content_stack: gtk::TemplateChild<gtk::Stack>,
        #[template_child]
        search_entry: gtk::TemplateChild<gtk::SearchEntry>,
        #[template_child]
        search_bar: gtk::TemplateChild<gtk::SearchBar>,
        #[template_child(id = "leaflet")]
        pub diary_leaflet: gtk::TemplateChild<adw::Leaflet>,
        #[template_child]
        sidebar_listview: gtk::TemplateChild<gtk::ListView>,
        #[template_child]
        title_entry: gtk::TemplateChild<gtk::Entry>,
        #[template_child]
        editor: gtk::TemplateChild<gtk::TextView>,
        #[template_child]
        pub calendar_button: gtk::TemplateChild<adw::ButtonContent>,

        unlock_fail_counter: Cell<u32>,
        pub selected_date: Cell<Option<NaiveDate>>,
        pub dek: Cell<EncryptionKey>,

        pub model: OnceCell<Storage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MainWindow {
        const NAME: &'static str = "BiographyMainWindow";
        type Type = super::MainWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
            utils::CommonCallbacks::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MainWindow {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            // Setup actions
            obj.setup_actions();

            // Load latest window state
            obj.load_window_size();

            // Save diary every 10 secs
            glib::timeout_add_seconds_local(
                10,
                clone!(@weak obj as win => @default-return Continue(false), move || {
                    win.imp().save_entry();
                    Continue(true)
                }),
            );

            if validate_password(None) {
                self.unlock_diary(None)
            }
        }
    }

    impl WidgetImpl for MainWindow {}
    impl WindowImpl for MainWindow {
        // Save window state on delete event
        fn close_request(&self, window: &Self::Type) -> gtk::Inhibit {
            if let Err(err) = window.save_window_size() {
                log::warn!("Failed to save window state, {}", &err);
            }

            // Unload diary
            self.unload_entry();

            // Pass close request on to the parent
            self.parent_close_request(window)
        }
    }

    impl ApplicationWindowImpl for MainWindow {}
    impl AdwApplicationWindowImpl for MainWindow {}

    #[gtk::template_callbacks]
    impl MainWindow {
        pub fn start_search(&self) {
            self.search_bar.set_search_mode(true);
            self.search_entry.grab_focus();
        }

        fn unlock_diary(&self, password: Option<&str>) {
            self.dek.set(load_key(password));
            self.model.set(Storage::new(self.dek.get())).unwrap();

            let selection = gtk::SingleSelection::builder()
                .model(self.model.get().unwrap())
                .autoselect(false)
                .can_unselect(false)
                .build();

            let win = self.instance();
            selection.connect_selection_changed(clone!(@weak win => move |selection, _, _| {
                glib::idle_add_local_once(clone!(@weak win, @weak selection => move || {
                    let win = win.imp();
                    let entry = selection
                        .selected_item()
                        .expect("Failed to get selected entry")
                        .downcast::<glib::BoxedAnyObject>()
                        .expect("Failed to cast selected item to BoxedAnyObject");
                    let date = entry
                        .borrow::<EntryMetadata>()
                        .date;
                    let entry = win
                        .model
                        .get()
                        .unwrap()
                        .load(date)
                        .expect("Failed to load entry");

                    win.unload_entry();

                    // Mark current entry as undefined
                    // to prevent saving after title changes
                    win.selected_date.take();

                    win.title_entry.set_text(&entry.title);
                    win.editor.buffer().set_text(&entry.text);
                    win.calendar_button.set_label(&entry.date.format("%x").to_string());

                    if entry.title.is_empty() {
                        win.title_entry.grab_focus_without_selecting();
                    } else {
                        win.editor.grab_focus();
                    }
                    win.diary_leaflet.set_visible_child_name("editor");

                    win.selected_date.set(Some(entry.date));
                }));
            }));

            // Save entry when title changed.
            // The main reason to do it is to update entry
            // title in a sidebar when title changed.
            self.title_entry
                .connect_changed(clone!(@weak win => move |_| {
                    win.imp().save_entry()
                }));

            let factory = gtk::SignalListItemFactory::new();

            factory.connect_setup(|_, item| {
                item.set_child(Some(&adw::ActionRow::builder().title_lines(1).build()))
            });

            factory.connect_bind(clone!(@weak win => move |_, item| {
                let row = item
                    .child()
                    .expect("Failed to get item child")
                    .downcast::<adw::ActionRow>()
                    .expect("Failed to cast item child to AdwActionRow");
                let entry = item
                    .item()
                    .expect("Failed to get item value")
                    .downcast::<glib::BoxedAnyObject>()
                    .expect("Failed to cast item value to BoxedAnyObject");

                let date = entry.borrow::<EntryMetadata>().date
                    .format("%x")
                    .to_string();
                let title = entry.borrow::<EntryMetadata>().title
                    .to_string();

                if title.trim().is_empty() {
                    row.set_title(&date);
                    row.set_subtitle("");
                } else {
                    row.set_title(&title);
                    row.set_subtitle(&date);
                }
            }));

            self.sidebar_listview.set_factory(Some(&factory));
            self.sidebar_listview.set_model(Some(&selection));
            self.content_stack.set_visible_child_name("diary");
            self.editor.grab_focus();

            let storage = self.model.get().expect("Failed to get storage");
            storage.set_selection(&selection);
            storage.select_date(chrono::Local::today().naive_local());
            selection.set_autoselect(true);
        }

        #[template_callback]
        fn password_confirmed(&self) {
            if validate_password(Some(self.password_row.text().as_str())) {
                self.unlock_diary(Some(&self.password_row.text()));
                return;
            }

            let list = self.password_listbox.get();
            let row = list.row_at_index(0).expect("Failed to get password row");

            let animation_target = adw::CallbackAnimationTarget::new(Some(Box::new(glib::clone!(
                @weak list => move |shift| {
                    let shift = (shift * 10.0).round() as i32;
                    list.set_margin_start(10 + shift);
                    list.set_margin_end(10 - shift);
                }
            ))));
            let animation = adw::TimedAnimation::builder()
                .widget(&list)
                .value_from(0.0)
                .value_to(1.0)
                .alternate(true)
                .repeat_count(4)
                .duration(100)
                .easing(adw::Easing::EaseInOutBounce)
                .target(&animation_target)
                .build();

            row.add_css_class("error");
            animation.play();

            let counter = &self.unlock_fail_counter;
            if counter.replace(counter.get() + 1) >= 2 {
                self.delete_diary_revealer.set_reveal_child(true);
            }
        }

        fn selected_entry(&self) -> Option<Entry> {
            self.selected_date.get().map(|date| {
                let buffer = self.editor.buffer();
                let (start, end) = buffer.bounds();

                Entry {
                    title: self.title_entry.text().into(),
                    text: buffer.text(&start, &end, true).into(),
                    date,
                }
            })
        }

        /// Save current entry.
        fn save_entry(&self) {
            if let Some(entry) = self.selected_entry() {
                self.model
                    .get()
                    .expect("Failed to get storage model")
                    .update_entry(entry)
                    .expect("Failed to save entry");
            }
        }

        /// Unload current entry.
        ///
        /// The main difference from saving is that
        /// unloading deletes entry if it's empty.
        fn unload_entry(&self) {
            if let Some(entry) = self.selected_entry() {
                self.model
                    .get()
                    .expect("Failed to get storage model")
                    .unload_entry(entry)
                    .expect("Failed to save entry");
            }
        }
    }
}

glib::wrapper! {
    pub struct MainWindow(ObjectSubclass<imp::MainWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl MainWindow {
    pub fn new(app: &Application) -> Self {
        glib::Object::new(&[("application", app)]).expect("Failed to create BiographyMainWindow")
    }

    fn setup_actions(&self) {
        let group = gio::SimpleActionGroup::new();

        declare_action!(
            "start-search" in group,
            clone!(
                @weak self as win => move |_, _| {
                    win.imp().start_search();
                }
            )
        );

        declare_action!(
            "go-back" in group,
            clone!(
                @weak self as win => move |_, _| {
                    win.imp().diary_leaflet.set_visible_child_name("sidebar");
                }
            )
        );

        declare_action!(
            "select-date" in group,
            clone!(
                @weak self as win => move |_, _| {
                    let calendar_window = Calendar::new(&win);

                    calendar_window.present();
                }
            )
        );

        declare_action!(
            "today" in group,
            clone!(
                @weak self as win => move |_, _| {
                    win.select_date(Local::today().naive_local())
                }
            )
        );

        declare_action!(
            "preferences" in group,
            clone!(
                @weak self as win => move |_, _| {
                    let preferences = Preferences::new(&win);

                    preferences.present();
                }
            )
        );

        self.insert_action_group("diary", Some(&group));
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let (width, height) = self.default_size();

        let settings = get_settings();

        settings.set_int("window-width", width)?;
        settings.set_int("window-height", height)?;
        settings.set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let settings = get_settings();

        let width = settings.int("window-width");
        let height = settings.int("window-height");
        let is_maximized = settings.boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }

    pub fn select_date(&self, date: NaiveDate) {
        let storage = self.imp().model.get().expect("Failed to get diary model");
        storage.select_date(date);
    }

    /// Get currently selected date.
    pub fn selected_date(&self) -> Option<NaiveDate> {
        self.imp().selected_date.get()
    }

    /// Get data encryption key.
    pub fn dek(&self) -> EncryptionKey {
        self.imp().dek.get()
    }
}
