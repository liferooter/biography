// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::clone;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::declare_action;
use crate::utils::secrets::change_password;

use super::preferences::Preferences;

mod imp {
    use std::cell::Cell;

    use crate::utils::{self, secrets::validate_password};

    use super::*;

    use gtk::CompositeTemplate;
    use once_cell::unsync::OnceCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/io/gnome/gitlab/liferooter/Biography/ui/password-page.ui")]
    pub struct PasswordPage {
        #[template_child]
        old_password: gtk::TemplateChild<gtk::Editable>,
        #[template_child]
        new_password: gtk::TemplateChild<gtk::Editable>,
        #[template_child]
        confirm_password: gtk::TemplateChild<gtk::Editable>,
        #[template_child]
        set_password: gtk::TemplateChild<gtk::Button>,

        pub prefs: OnceCell<Preferences>,
        pub is_set: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PasswordPage {
        const NAME: &'static str = "BiographyPasswordPage";
        type Type = super::PasswordPage;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
            utils::CommonCallbacks::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template()
        }
    }

    impl ObjectImpl for PasswordPage {
        fn constructed(&self, obj: &Self::Type) {
            obj.setup_actions();

            // FIXME: use `AdwEntryRow::activated`.
            // For more details see: https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/595
            self.old_password
                .delegate()
                .unwrap()
                .downcast::<gtk::Text>()
                .unwrap()
                .connect_activate(clone!(@weak obj => move |_| {
                    let page = obj.imp();
                    page.validate_old_password();
                    page.new_password.grab_focus();
                }));
            self.new_password
                .delegate()
                .unwrap()
                .downcast::<gtk::Text>()
                .unwrap()
                .connect_activate(clone!(@weak obj => move |_| {
                    let page = obj.imp();
                    page.validate_old_password();
                    page.validate_new_password();
                    obj.imp().confirm_password.grab_focus();
                }));
            self.confirm_password
                .delegate()
                .unwrap()
                .downcast::<gtk::Text>()
                .unwrap()
                .connect_activate(clone!(@weak obj => move |_| {
                    obj.imp().set_password.activate();
                }));

            // Don't ask for old password if password is not set
            if validate_password(None) {
                self.old_password.set_visible(false);
            }
        }
    }

    impl WidgetImpl for PasswordPage {
        fn unmap(&self, widget: &Self::Type) {
            self.parent_unmap(widget);

            if !self.is_set.get() {
                self.prefs.get().unwrap().set_password_state(false);
            }
        }
    }

    impl BinImpl for PasswordPage {}

    #[gtk::template_callbacks]
    impl PasswordPage {
        pub fn validate_old_password(&self) -> bool {
            let password = if self.old_password.is_visible() {
                Some(self.old_password.text())
            } else {
                None
            };
            let is_correct = validate_password(password.as_deref());

            if !is_correct {
                self.old_password.add_css_class("error");
            }

            is_correct
        }

        pub fn validate_new_password(&self) -> bool {
            let is_equal = self.new_password.text() == self.confirm_password.text();

            if !is_equal {
                self.confirm_password.add_css_class("error");
            }

            is_equal
        }

        pub fn password(&self) -> String {
            self.new_password.text().to_string()
        }
    }
}

glib::wrapper! {
    pub struct PasswordPage(ObjectSubclass<imp::PasswordPage>)
        @extends gtk::Widget, adw::Bin;
}

impl PasswordPage {
    pub fn new(prefs: &Preferences) -> Self {
        let object: Self = glib::Object::new(&[]).expect("Failet to create BiograhyPasswordPage");
        object.imp().prefs.set(prefs.clone()).unwrap();
        object
    }

    pub fn setup_actions(&self) {
        let group = gio::SimpleActionGroup::new();

        declare_action!(
            "apply" in group,
            clone!(@weak self as page => move |_, _| {
                let page = page.imp();
                // Validate passwords
                let old_password_valid = page.validate_old_password();
                let confirmation_valid = page.validate_new_password();
                if !old_password_valid || !confirmation_valid {
                    return;
                }

                let password = page.password();

                let prefs = page
                    .prefs
                    .get()
                    .unwrap();

                change_password(prefs.main_window().dek(), Some(&password));
                page.is_set.set(true);
                prefs.set_password_state(true);
                prefs.close_subpage();
            })
        );

        self.insert_action_group("password", Some(&group));
    }
}
