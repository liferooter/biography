// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::array::TryFromSliceError;

use aes::{
    cipher::{generic_array::GenericArray, BlockDecrypt, BlockEncrypt, KeyInit},
    Aes256,
};
use argon2::{
    password_hash::{self, PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Argon2,
};
use rand::prelude::*;
use rand_core::OsRng;

use crate::error_type;

pub type EncryptionKey = [u8; argon2::Params::DEFAULT_OUTPUT_LEN];

error_type! {
    pub error Error {
        CantHashPassword(password_hash::Error) => "Failed to hash password",
        InvalidBufferSize(TryFromSliceError) => "Invalid buffer size",
    }
}

type Result<T> = std::result::Result<T, Error>;

pub fn validate_password_by_hash(password: &str, hash: &str) -> Result<()> {
    let password_hash = PasswordHash::new(hash)?;
    Ok(Argon2::default().verify_password(password.as_bytes(), &password_hash)?)
}

#[allow(unused)]
pub fn hash_password(password: &str) -> Result<String> {
    let salt = SaltString::generate(&mut OsRng);
    let argon2 = Argon2::default();
    Ok(argon2
        .hash_password(password.as_bytes(), &salt)?
        .to_string())
}

pub fn derive_key(password: &str, salt: &str) -> Result<EncryptionKey> {
    Ok(Argon2::default()
        .hash_password(password.as_ref(), salt)?
        .hash
        .unwrap()
        .as_ref()
        .try_into()?)
}

pub fn encrypt_dek(password: &str, mut dek: EncryptionKey) -> Result<(EncryptionKey, String)> {
    let salt = SaltString::generate(&mut OsRng);
    let kek = derive_key(password, salt.as_str())?;
    let kek = GenericArray::from(kek);

    let cipher = Aes256::new(&kek);
    cipher.encrypt_block(GenericArray::from_mut_slice(&mut dek[..16]));
    cipher.encrypt_block(GenericArray::from_mut_slice(&mut dek[16..]));

    Ok((dek, salt.to_string()))
}

pub fn generate_dek() -> EncryptionKey {
    OsRng.gen()
}

pub fn decrypt_dek(dek: EncryptionKey, password: &str, salt: &str) -> Result<EncryptionKey> {
    let kek = derive_key(password, salt)?;
    let cipher = Aes256::new(&GenericArray::from(kek));
    let mut dek = GenericArray::from(dek);
    cipher.decrypt_block(GenericArray::from_mut_slice(&mut dek[..16]));
    cipher.decrypt_block(GenericArray::from_mut_slice(&mut dek[16..]));
    Ok(dek.as_slice().try_into().unwrap())
}

pub trait Encryptable: Sized {
    fn encrypt(&self, encryption: EncryptionKey) -> Result<Self>;
    fn decrypt(&self, encryption: EncryptionKey) -> Result<Self>;
}

impl Encryptable for Vec<u8> {
    fn encrypt(&self, dek: EncryptionKey) -> Result<Self> {
        let cipher = Aes256::new(&GenericArray::from(dek));
        let mut blocks: Vec<_> = self
            .chunks(16)
            .map(|block| {
                let mut block = block.to_vec();
                while block.len() < 16 {
                    block.push(0);
                }
                let block: [u8; 16] = block.try_into().unwrap();
                GenericArray::from(block)
            })
            .collect();
        cipher.encrypt_blocks(&mut blocks);
        Ok(blocks.into_iter().flatten().collect())
    }

    fn decrypt(&self, dek: EncryptionKey) -> Result<Self> {
        let dek = GenericArray::from(dek);
        let cipher = Aes256::new(&dek);
        let mut blocks: Vec<_> = self
            .chunks(16)
            .map(|block| {
                // Encrypted blocks are already aligned
                let block: [u8; 16] = block.try_into().unwrap();
                GenericArray::from(block)
            })
            .collect();
        cipher.decrypt_blocks(&mut blocks);
        let mut bytes: Vec<u8> = blocks.into_iter().flatten().collect();
        while bytes.last() == Some(&0) {
            bytes.pop();
        }

        Ok(bytes)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encryption() {
        let data = "Hello world!";
        let encryption = generate_dek();
        assert_eq!(
            &data
                .as_bytes()
                .to_vec()
                .encrypt(encryption)
                .unwrap()
                .decrypt(encryption)
                .unwrap(),
            data.as_bytes()
        );
    }
}
