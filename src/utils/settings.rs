// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::ops::Deref;

use gtk::gio;
use once_cell::unsync::Lazy;

use crate::config::APP_ID;

thread_local! {
    static SETTINGS: Lazy<gio::Settings> = Lazy::new(|| gio::Settings::new(APP_ID))
}

/// Get application settings.
pub fn get_settings() -> gio::Settings {
    SETTINGS.with(|settings| settings.deref().clone())
}
