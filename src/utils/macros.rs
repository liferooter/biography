// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#[macro_export]
macro_rules! declare_action {
    (
        $name:literal $(($arg_type:ident ))? in $group:expr,
        $callback:expr
    ) => {{
        let action = gtk::gio::SimpleAction::new(
            $name,
            None
                $(.or(Some(gtk::glib::VariantTy::$arg_type)))?
        );
        action.connect_activate($callback);
        $group.add_action(&action);
    }};
    (
        $name:literal $(( $arg_type:ident ))? : $state:ident in $group:expr,
        $callback:expr
    ) => {{
        let action = gtk::gio::SimpleAction::new_stateful(
            $name,
            None
                $(.or(Some(gtk::glib::VariantTy::$arg_type)))?,
            &gtk::glib::ToVariant::to_variant(&$state)
        );
        action.connect_activate($callback);
        $group.add_action(&action);
    }};
}

#[macro_export]
macro_rules! error_type {
    (
        $visibility:vis error $name:ident {
            $(
                $wrapper:ident $(($inner:ty))? => $message:literal $(,)?
            )*
        }
    ) => {
        #[derive(Debug)]
        $visibility enum $name {
            $(
                $wrapper$(($inner))?,
            )*
        }

        impl $name {
            #[allow(unreachable_patterns)]
            fn message_postfix(&self) -> String {
                match self {
                    $($(Self::$wrapper(err) => format!(": {}", <$inner as std::string::ToString>::to_string(err)),)?)*
                    _ => "".to_string()
                }
            }
        }

        impl std::fmt::Display for $name {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                match self {
                    $(
                        Self::$wrapper {..} => write!(
                            f,
                            "{}{}",
                            $message,
                            self.message_postfix()
                        ),
                    )*
                }
            }
        }

        $($(
        impl From<$inner> for $name {
            fn from(err: $inner) -> Self {
                Self::$wrapper(err)
            }
        }
        )?)*

        impl std::error::Error for $name {}
    };
}
