// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gio::prelude::*;
use gtk::gio;

use crate::utils::get_settings;

use super::crypto::{self, decrypt_dek, encrypt_dek, generate_dek, hash_password, EncryptionKey};

/// Get encrypted encryption key.
fn get_encrypted_key() -> Option<EncryptionKey> {
    let key_string = get_settings().string("encryption-key");
    if key_string.is_empty() {
        return None;
    }

    let bytes_vec = hex::decode(key_string).ok()?;
    bytes_vec.as_slice().try_into().ok()
}

/// Store encrypted encryption key in secret service.
fn save_encrypted_key(key: EncryptionKey) -> Result<(), gtk::glib::BoolError> {
    get_settings().set_string("encryption-key", &hex::encode(key))
}

pub fn validate_password(password: Option<&str>) -> bool {
    let password_hash = get_settings().string("password-hash");
    if password_hash.is_empty() {
        // No password set
        password.is_none()
    } else if let Some(password) = password {
        // Password set and given,
        // so validate it cryptographically.
        crypto::validate_password_by_hash(password, &password_hash).is_ok()
    } else {
        // Password is set, but not given.
        // It just can't be valid.
        false
    }
}

/// Load key from storage, create new if not set.
pub fn load_key(password: Option<&str>) -> EncryptionKey {
    let key = get_encrypted_key();
    match (key, password) {
        (None, None) => {
            // Key is not yet generated,
            // so generate it.
            let dek = generate_dek();
            // Not password is set, so
            // store raw DEK in secret service.
            save_encrypted_key(dek).expect("Failed to store encryption key");
            dek
        }
        (Some(key), None) => {
            // No password is set,
            // so raw DEK is stored.
            // Just return it.
            key
        }
        (Some(key), Some(password)) => {
            // DEK is encrypted by password,
            // so decrypt it.

            // Load salt from GSettings
            let salt = get_settings().string("encryption-salt");

            decrypt_dek(key, password, &salt).expect("Failed to decrypt encryption key")
        }
        (None, Some(_)) => {
            // Oh no!
            // Password is set, so key is expected
            // to be presented, but it isn't.
            // So just crash.
            panic!("Key is expected, but not found")
        }
    }
}

/// Change password and update entryction key.
pub fn change_password(dek: EncryptionKey, new_password: Option<&str>) {
    let (key, salt) = if let Some(password) = new_password {
        // If password is set, encrypt DEK with it and save.
        encrypt_dek(password, dek).expect("Failed to generate new key")
    } else {
        // If password is not set, save raw DEK to secret service.
        (dek, String::from(""))
    };

    // Save salt to GSettings
    get_settings()
        .set_string("encryption-salt", &salt)
        .expect("Failed to save salt to GSettings");

    // Save key to secret service
    save_encrypted_key(key).expect("Failet to save encryption key");

    // Save password hash
    get_settings()
        .set_string(
            "password-hash",
            &if let Some(password) = &new_password {
                hash_password(password).expect("Failed to hash password")
            } else {
                String::new()
            },
        )
        .expect("Failed to save password hash");
}
