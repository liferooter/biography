// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::prelude::*;

pub struct CommonCallbacks;

#[gtk::template_callbacks]
impl CommonCallbacks {
    #[template_callback]
    fn grab_focus(widget: gtk::Widget) {
        widget.grab_focus();
    }

    #[template_callback]
    fn remove_error(widget: gtk::Widget) {
        widget.remove_css_class("error");
    }

    #[template_callback]
    fn activate(widget: gtk::Widget) {
        widget.activate();
    }
}
