// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

/// Entry metadata.
///
/// This structure is used to store entries in
/// storage model and serialize it into encrypted
/// diary metadata file.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EntryMetadata {
    pub title: String,
    pub date: NaiveDate,
    pub filename: String,
}

/// Entry object.
///
/// This structure is used to store, update and get
/// diary entry including its title and text.
#[derive(Debug)]
pub struct Entry {
    pub title: String,
    pub text: String,
    pub date: NaiveDate,
}
