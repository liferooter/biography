// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::path::PathBuf;

pub fn get_data_dir() -> Option<PathBuf> {
    Some(dirs::data_dir()?.join("biography"))
}

pub fn get_entries_dir() -> Option<PathBuf> {
    Some(get_data_dir()?.join("entries"))
}

pub fn get_entry_path(filename: &str) -> Option<PathBuf> {
    Some(get_entries_dir()?.join(filename))
}
