// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::ops::Deref;

use chrono::NaiveDate;
use gtk::{gio, glib, SelectionModel};

use crate::diary::{metadata::Result, Entry, EntryMetadata};
use crate::utils::crypto::EncryptionKey;

use glib::prelude::*;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

mod imp {
    use super::*;

    use std::cell::{Cell, RefCell};
    use std::fs;

    use chrono::NaiveDate;
    use compression::prelude::*;
    use gtk::prelude::ListModelExt;
    use once_cell::unsync::OnceCell;
    use sha2::{Digest, Sha256};

    use crate::diary::metadata::{load_metadata, save_metadata, DiaryError};
    use crate::diary::paths::get_entry_path;
    use crate::utils::crypto::Encryptable;

    #[derive(Debug, Default, Clone)]
    pub struct Storage {
        metadata: RefCell<Vec<EntryMetadata>>,
        encryption_key: Cell<EncryptionKey>,
        list_model: OnceCell<super::Storage>,
        pub selection: OnceCell<SelectionModel>,
    }

    impl Storage {
        pub fn load(&self, encryption: EncryptionKey) -> Result<()> {
            let entries = load_metadata(encryption)?;

            self.metadata.replace(entries);
            self.encryption_key.set(encryption);

            Ok(())
        }

        pub fn len(&self) -> usize {
            self.metadata.borrow().len()
        }

        pub fn get_nth(&self, n: usize) -> Option<EntryMetadata> {
            self.metadata.borrow().get(n).cloned()
        }

        pub fn index(&self, date: NaiveDate) -> usize {
            let index = self
                .metadata
                .borrow()
                .binary_search_by(|d| date.cmp(&d.date));
            index.unwrap_or_else(|index| {
                self.insert_entry(date)
                    .expect("Failed to insert entry for date");
                index
            })
        }

        pub fn load_entry(&self, date: NaiveDate) -> Result<Entry> {
            let index = self
                .metadata
                .borrow()
                .binary_search_by(|entry| date.cmp(&entry.date));
            let entry: EntryMetadata = match index {
                Ok(index) => self.metadata.borrow()[index].clone(),
                Err(index) => {
                    self.insert_entry(date)?;
                    self.metadata.borrow()[index].clone()
                }
            };

            let path = get_entry_path(&entry.filename).ok_or(DiaryError::FileNotFound)?;

            let content = if path.exists() {
                fs::read(path)?
                    .decrypt(self.encryption_key.get())?
                    .decode(&mut GZipDecoder::new())
                    .collect::<std::result::Result<Vec<u8>, _>>()?
            } else {
                Vec::new()
            };
            let text = String::from_utf8(content)?;

            Ok(Entry {
                date,
                title: entry.title,
                text,
            })
        }

        pub fn insert_entry(&self, date: NaiveDate) -> Result<()> {
            let filename = {
                let mut hasher = Sha256::new();
                hasher.update(
                    date.to_string()
                        .into_bytes()
                        .encrypt(self.encryption_key.get())?,
                );
                hex::encode(hasher.finalize())
            };

            let entry = EntryMetadata {
                title: String::new(),
                filename,
                date,
            };
            let index = self
                .metadata
                .borrow()
                .binary_search_by(|entry| date.cmp(&entry.date))
                .unwrap_err();
            self.metadata.borrow_mut().insert(index, entry);

            self.list_model
                .get()
                .unwrap()
                .items_changed(index as u32, 0, 1);

            Ok(())
        }

        pub fn remove_entry(&self, date: NaiveDate) -> Result<()> {
            let index = self
                .metadata
                .borrow()
                .binary_search_by(|entry| date.cmp(&entry.date))
                .map_err(|_| DiaryError::NoEntryFound)?;
            let filename = self
                .metadata
                .borrow()
                .get(index)
                .ok_or(DiaryError::NoEntryFound)?
                .filename
                .clone();
            let filename = get_entry_path(&filename).ok_or(DiaryError::FileNotFound)?;

            if filename.exists() {
                fs::remove_file(filename)?;
            }

            self.metadata.borrow_mut().remove(index);

            self.list_model
                .get()
                .expect("Failed to get list model")
                .items_changed(index as u32, 1, 0);

            self.save_metadata()?;

            Ok(())
        }

        pub fn update_entry(&self, entry: Entry) -> Result<()> {
            let index = self
                .metadata
                .borrow()
                .binary_search_by(|e| entry.date.cmp(&e.date))
                .map_err(|_| DiaryError::NoEntryFound)?;
            self.metadata.borrow_mut()[index].title = entry.title.clone();

            let content = entry
                .text
                .clone()
                .into_bytes()
                .encode(&mut GZipEncoder::new(), Action::Finish)
                .collect::<std::result::Result<Vec<u8>, _>>()?
                .encrypt(self.encryption_key.get())?;

            let filename = self.metadata.borrow()[index].filename.clone();

            fs::write(
                get_entry_path(&filename).ok_or(DiaryError::FileNotFound)?,
                content,
            )?;

            let index = self
                .metadata
                .borrow()
                .binary_search_by(|e| entry.date.cmp(&e.date))
                .map_err(|_| DiaryError::NoEntryFound)?;
            self.list_model
                .get()
                .expect("Failed to get list model")
                .items_changed(index as u32, 1, 1);

            self.save_metadata()
        }

        pub fn unload_entry(&self, entry: Entry) -> Result<()> {
            if entry.title.trim().is_empty() && entry.text.trim().is_empty() {
                self.remove_entry(entry.date)
            } else {
                self.update_entry(entry)
            }
        }

        pub fn save_metadata(&self) -> Result<()> {
            save_metadata(self.encryption_key.get(), &self.metadata.borrow())
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Storage {
        const NAME: &'static str = "DiaryStorage";
        type Type = super::Storage;
        type ParentType = glib::Object;
        type Interfaces = (gio::ListModel,);
    }

    impl ObjectImpl for Storage {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            self.list_model
                .set(obj.clone())
                .expect("Failed to set list model");
        }
    }

    impl ListModelImpl for Storage {
        fn item_type(&self, _: &Self::Type) -> glib::Type {
            glib::BoxedAnyObject::static_type()
        }

        fn n_items(&self, _: &Self::Type) -> u32 {
            self.len() as u32
        }

        fn item(&self, _: &Self::Type, position: u32) -> Option<glib::Object> {
            self.get_nth(position as usize)
                .map(|entry| glib::BoxedAnyObject::new(entry).upcast())
        }
    }
}

glib::wrapper! {
    pub struct Storage(ObjectSubclass<imp::Storage>)
        @implements gio::ListModel;
}

impl Deref for Storage {
    type Target = imp::Storage;

    fn deref(&self) -> &Self::Target {
        self.imp()
    }
}

impl Storage {
    pub fn new(encryption: EncryptionKey) -> Self {
        let object: Self = glib::Object::new(&[]).expect("Failed to create storage");

        object.imp().load(encryption).expect("Failed to load diary");

        object
    }

    pub fn set_selection(&self, model: &impl IsA<SelectionModel>) {
        self.imp().selection.set(model.clone().upcast()).unwrap();
    }

    pub fn load(&self, date: NaiveDate) -> Result<Entry> {
        self.imp().load_entry(date)
    }

    pub fn select_date(&self, date: NaiveDate) {
        let index = self.imp().index(date) as u32;

        self.imp().selection.get().unwrap().select_item(index, true);
    }
}
