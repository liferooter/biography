// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    fs::{self, create_dir_all},
    io,
};

use compression::prelude::*;

use crate::{
    diary::EntryMetadata,
    error_type,
    utils::crypto::{self, Encryptable, EncryptionKey},
};

use super::paths::{get_data_dir, get_entries_dir};

error_type! {
    pub error DiaryError {
        IOError(io::Error) => "I/O error",
        Encryption(crypto::Error) => "encryption error",
        Decoding(std::string::FromUtf8Error) => "decoding error",
        Compression(CompressionError) => "compression error",
        Json(serde_json::Error) => "JSON error",
        NoEntryFound => "no entry found for date",
        FileNotFound => "entry file is not found"
    }
}

pub type Result<T> = std::result::Result<T, DiaryError>;

pub fn load_metadata(encryption_info: EncryptionKey) -> Result<Vec<EntryMetadata>> {
    let entries_dir = get_entries_dir().expect("Failed to get entries dir");

    if !entries_dir.exists() {
        create_dir_all(entries_dir)?;
    }

    let path = get_data_dir()
        .expect("Failed to find data directory")
        .join("metadata.json.gz.enc");

    if !path.exists() {
        return Ok(Vec::new());
    }

    let content = fs::read(path)?.decrypt(encryption_info)?;

    let json: Vec<u8> = content
        .into_iter()
        .decode(&mut GZipDecoder::new())
        .collect::<std::result::Result<_, _>>()?;

    Ok(serde_json::from_str(&String::from_utf8(json)?)?)
}

pub fn save_metadata(encryption_info: EncryptionKey, entries: &Vec<EntryMetadata>) -> Result<()> {
    let json = serde_json::to_string(&entries)?;

    let content = json
        .bytes()
        .encode(&mut GZipEncoder::new(), Action::Finish)
        .collect::<std::result::Result<Vec<_>, _>>()?
        .encrypt(encryption_info)?;

    let entries_dir = get_entries_dir().expect("Failed to get entries dir");

    if !entries_dir.exists() {
        create_dir_all(entries_dir)?;
    }

    let path = get_data_dir()
        .expect("Failed to find data directory")
        .join("metadata.json.gz.enc");

    fs::write(path, content)?;

    Ok(())
}
